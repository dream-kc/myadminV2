<?php
declare (strict_types = 1);

namespace app\api\controller;
use app\api\BaseController;
use app\api\middleware\Auth;
use app\api\service\JwtAuth;
use hg\apidoc\annotation as Apidoc;
use think\facade\Db;

/**
 * @Apidoc\Title("基础示例")
 */
class Index extends BaseController
{

    protected $middleware = [
        Auth::class => ['except' 	=> ['index1','login','reg'] ]
    ];

    /**
     * @Apidoc\Title("基础的注释方法")
     * @Apidoc\Desc("最基础的接口注释写法")
     * @Apidoc\Method("GET")
     * @Apidoc\Author("HG-CODE")
     * @Apidoc\Tag("测试")
     * @Apidoc\Param("username", type="abc",require=true, desc="用户名")
     * @Apidoc\Param("password", type="string",require=true, desc="密码")
     * @Apidoc\Param("phone", type="string",require=true, desc="手机号")
     * @Apidoc\Param("sex", type="int",default="1",desc="性别" )
     * @Apidoc\Returned("id", type="int", desc="用户id")
     */
    public function index()
    {
        return '您好！这是一个[api]示例应用';
    }

    /**
     * @Apidoc\Title ("会员登录")
     * @Apidoc\Desc ("用于会员登录接口")
     * @Apidoc\Method ("POST")
     * @Apidoc\Author ("MYKC")
     * @Apidoc\Tag ("登录")
     * @Apidoc\Param ("username",type="string",require=true,desc="用户名")
     * @Apidoc\Param ("password",type="string",require=true,desc="密码")
     * @Apidoc\Returned ("token",type="string",desc="token")
     */
    public function login()
    {
        $param = get_params();
        if(empty($param['username']) || empty($param['password'])){
            $this->apiError('参数错误');
        }
        // 校验用户名密码
        $user = Db::name('User')->where(['username' => $param['username']])->find();
        if (empty($user)) {
            $this->apiError('帐号或密码错误');
        }
        $param['pwd'] = set_password($param['password'], $user['salt']);
        if ($param['pwd'] !== $user['password']) {
            $this->apiError('帐号或密码错误');
        }
        if ($user['status'] == -1) {
            $this->apiError('该用户禁止登录,请于平台联系');
        }
        $data = [
            'last_login_time' => time(),
            'last_login_ip' => request()->ip(),
            'login_num' => $user['login_num'] + 1,
        ];
        $res = Db::name('user')->where(['id' => $user['id']])->update($data);
        if($res){
            //获取jwt的句柄
            $jwtAuth = JwtAuth::getInstance();
            $token = $jwtAuth->setUid($user['id'])->encode()->getToken();
            add_user_log('api', '登录');
            $this->apiSuccess('登录成功',['token' => $token]);
        }
    }

    /**
     * @Apidoc\Title ("会员注册")
     * @Apidoc\Desc ("用于会员注册接口")
     * @Apidoc\Method ("POST")
     * @Apidoc\Author ("MYKC")
     * @Apidoc\Tag ("注册")
     * @Apidoc\Param ("username",type="string",require=true,desc="用户名")
     * @Apidoc\Param ("pwd",type="string",require=true,desc="密码")
     */
    public function reg()
    {
        $param = get_params();
        if(empty($param['username']) || empty($param['pwd'])){
            $this->apiError('参数错误');
        }
        $user = Db::name('user')->where(['username' => $param['username']])->find();
        if (!empty($user)) {
            $this->apiError('该账户已经存在');
        }
        $param['salt'] = set_salt(20);
        $param['password'] = set_password($param['pwd'], $param['salt']);
        $param['register_time'] = time();
        $param['headimgurl'] = '/static/admin/images/icon.png';
        $param['register_ip'] = request()->ip();
        $char = mb_substr($param['username'], 0, 1, 'utf-8');
        $uid = Db::name('User')->strict(false)->field(true)->insertGetId($param);
        if($uid){
            add_user_log('api', '注册');
            $this->apiSuccess('注册成功');
        }else{
            $this->apiError('注册失败');
        }
    }
}
