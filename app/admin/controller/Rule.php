<?php

namespace app\admin\controller;

use app\admin\BaseController;
use app\admin\validate\RuleCheck;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\View;

class Rule extends BaseController
{
    public function index()
    {
        if (request()->isAjax()) {
            $rule = Db::name('adminRule')->order('create_time asc')->select();
            return $rule;
        } else {
            return view();
        }
    }

    //添加
    public function add()
    {
        return view('', ['pid' => get_params('pid')]);
    }

    public function edit()
    {
        $id = get_params("pid");
        $data = Db::name('AdminRule')->where(["id"=>$id])->find();
        View::assign('id', $id);
        View::assign('config', $data);
        return View::fetch();
    }

    //删除
    public function delete()
    {
        $id = get_params("id");
        $count = Db::name('AdminRule')->where(["pid" => $id])->count();
        if ($count > 0) {
            return to_assign(1, "该节点下还有子节点，无法删除");
        }
        if (Db::name('AdminRule')->delete($id) !== false) {
            clear_cache('adminRules');
            //add_log('delete', $id, []);
            return to_assign(0, "删除节点成功");
        } else {
            return to_assign(1, "删除失败");
        }
    }

    public function post_submit()
    {
        if (request()->isAjax()) {
            $param = get_params();
            if ($param['id'] > 0) {
                //$data[$param['field']] = $param['value'];
                //$data = $param['id'];
                if(!empty($data['title'])){
                    try {
                        validate(RuleCheck::class)->scene('edit_title')->check($data);
                    } catch (ValidateException $e) {
                        // 验证失败 输出错误信息
                        return to_assign(1, $e->getError());
                    }
                }
                if(!empty($data['src'])){
                    try {
                        validate(RuleCheck::class)->scene('edit_src')->check($data);
                    } catch (ValidateException $e) {
                        // 验证失败 输出错误信息
                        return to_assign(1, $e->getError());
                    }
                }
                Db::name('AdminRule')->where(["id"=>$param['id']])->update($param);
                //add_log('edit', $param['id'], $data);
            } else {
                try {
                    validate(RuleCheck::class)->scene('add')->check($param);
                } catch (ValidateException $e) {
                    // 验证失败 输出错误信息
                    return to_assign(1, $e->getError());
                }
                $rid = Db::name('AdminRule')->strict(false)->field(true)->insertGetId($param);
                //自动为系统所有者管理组分配新增的节点
                $group = Db::name('AdminGroup')->find(1);
                if (!empty($group)) {
                    $newGroup['id'] = 1;
                    $newGroup['rules'] = $group['rules'] . ',' . $rid;
                    Db::name('AdminGroup')->strict(false)->field(true)->update($newGroup);
                    //add_log('add', $rid, $param);
                }
            }
            // 删除后台节点缓存
            clear_cache('adminRules');
            return to_assign();
        }
    }
}