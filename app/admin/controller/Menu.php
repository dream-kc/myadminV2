<?php

namespace app\admin\controller;

use app\admin\BaseController;
use app\admin\validate\MenuCheck;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\View;

class Menu extends BaseController
{
    public function index()
    {
        if (request()->isAjax()) {
            $menu = Db::name('AdminMenu')->order('sort asc')->select();
            return $menu;
        } else {
            return view();
        }
    }

    public function delete()
    {
        $id = get_params('id');
        $count = Db::name('AdminMenu')->where(['pid' => $id])->count();
        if ($count > 0) {
            return to_assign(1, '该菜单下还有子菜单，无法删除');
        }
        if (Db::name('AdminMenu')->delete($id) !== false) {
            // 删除后台菜单缓存
            clear_cache('adminMenu');
            //add_log('delete', $id, []);
            return to_assign(0, '删除菜单成功');
        } else {
            return to_assign(1, '删除失败');
        }
    }

    public function add()
    {
        return view('', ['pid' => get_params('pid')]);
    }

    public function edit()
    {
        $id = get_params("pid");
        $data = Db::name('AdminMenu')->where(["id"=>$id])->find();
        View::assign('id', $id);
        View::assign('config', $data);
        return View::fetch();
    }

    public function post_submit()
    {
        if (request()->isAjax()) {
            $param = get_params();
            if ($param['id'] > 0) {
                //$data[$param['field']] = $param['value'];
                $data['id'] = $param['id'];
                if(!empty($data['title'])){
                    try {
                        validate(MenuCheck::class)->scene('edit')->check($data);
                    } catch (ValidateException $e) {
                        // 验证失败 输出错误信息
                        return to_assign(1, $e->getError());
                    }
                }
                Db::name('AdminMenu')->strict(false)->field(true)->update($param);
                //add_log('edit', $param['id'], $data);
            } else {
                try {
                    validate(MenuCheck::class)->scene('add')->check($param);
                } catch (ValidateException $e) {
                    // 验证失败 输出错误信息
                    return to_assign(1, $e->getError());
                }
                $mid = Db::name('AdminMenu')->strict(false)->field(true)->insertGetId($param);
                //自动为系统所有者管理组分配新增的菜单
                $group = Db::name('AdminGroup')->find(1);
                if (!empty($group)) {
                    $newGroup['id'] = 1;
                    $newGroup['menus'] = $group['menus'] . ',' . $mid;
                    Db::name('AdminGroup')->strict(false)->field(true)->update($newGroup);
                    //add_log('add', $mid, $param);
                }
            }
            // 删除后台菜单缓存
            clear_cache('adminMenu');
            return to_assign();
        }
    }
}