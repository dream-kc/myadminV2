<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\BaseController;
use think\facade\View;
class Index extends BaseController
{
    public function index()
    {
        $menu = get_admin_menus();
        View::assign('menu', $menu);
        return View::fetch();
    }

    public function console()
    {
        return View::fetch();
    }
}
