/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50734
 Source Host           : localhost:3306
 Source Schema         : sql_127_0_0_4

 Target Server Type    : MySQL
 Target Server Version : 50734
 File Encoding         : 65001

 Date: 04/02/2022 14:56:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for my_admin
-- ----------------------------
DROP TABLE IF EXISTS `my_admin`;
CREATE TABLE `my_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `pwd` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `salt` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` bigint(11) NULL DEFAULT 0,
  `desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  `last_login_time` int(11) NOT NULL DEFAULT 0,
  `login_num` int(11) NOT NULL DEFAULT 0,
  `last_login_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT 1 COMMENT '1正常,0禁止登录,-1删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin
-- ----------------------------
INSERT INTO `my_admin` VALUES (1, 'admin', '28d35a18149484e5403dd2d65fcd4f5b', '7kU3QiBW4yb5CgYMswL6', '超级管理员', '/static/admin/images/icon.png', 17725446221, '', 1643526015, 1643526015, 1643939064, 41, '127.0.0.1', 1);

-- ----------------------------
-- Table structure for my_admin_group
-- ----------------------------
DROP TABLE IF EXISTS `my_admin_group`;
CREATE TABLE `my_admin_group`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `status` int(1) NOT NULL DEFAULT 1,
  `rules` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则\",\"隔开',
  `menus` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限分组表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin_group
-- ----------------------------
INSERT INTO `my_admin_group` VALUES (1, '超级管理员', 1, '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,39,40,41,42,43,44,45,50,51,52,53,54', '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,14,15,16,17,18,19,20', '超级管理员，系统自动分配所有可操作权限及菜单。', 0, 0);
INSERT INTO `my_admin_group` VALUES (2, '测试角色', 1, '1,5,6,11,15,19,23,28,29,30', '1,5,6,7,8,9,10,11,12', '测试角色', 0, 0);

-- ----------------------------
-- Table structure for my_admin_group_access
-- ----------------------------
DROP TABLE IF EXISTS `my_admin_group_access`;
CREATE TABLE `my_admin_group_access`  (
  `uid` int(11) UNSIGNED NULL DEFAULT NULL,
  `group_id` int(11) NULL DEFAULT NULL,
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  UNIQUE INDEX `uid_group_id`(`uid`, `group_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限分组和管理员的关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin_group_access
-- ----------------------------
INSERT INTO `my_admin_group_access` VALUES (1, 1, 0, 0);

-- ----------------------------
-- Table structure for my_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `my_admin_log`;
CREATE TABLE `my_admin_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `type` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作类型',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作描述',
  `module` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块',
  `controller` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '控制器',
  `function` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '方法',
  `rule_menu` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '节点权限路径',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录ip',
  `param_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '操作数据id',
  `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参数json格式',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0删除 1正常',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台操作日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for my_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `my_admin_menu`;
CREATE TABLE `my_admin_menu`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `src` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `sort` int(11) NOT NULL DEFAULT 1 COMMENT '越大越靠前',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '后台菜单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin_menu
-- ----------------------------
INSERT INTO `my_admin_menu` VALUES (1, 0, '系统管理', '', 'mdi mdi-animation-outline', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (5, 1, '系统配置', 'admin/conf/index', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (6, 1, '功能菜单', 'admin/menu/index', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (7, 1, '功能节点', 'admin/rule/index', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (8, 1, '权限角色', 'admin/role/index', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (9, 1, '管 理 员', 'admin/admin/index', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (10, 1, '操作日志', 'admin/admin/log', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (11, 1, '数据备份', 'admin/database/database', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (12, 1, '数据还原', 'admin/database/backuplist', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (14, 0, '用户管理', '', 'mdi mdi-account-supervisor', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (15, 14, '用户列表', 'admin/user/index', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (16, 14, '操作记录', 'admin/user/record', '', 1, 0, 0);
INSERT INTO `my_admin_menu` VALUES (17, 14, '用户日志', 'admin/user/log', '', 1, 0, 0);

-- ----------------------------
-- Table structure for my_admin_rule
-- ----------------------------
DROP TABLE IF EXISTS `my_admin_rule`;
CREATE TABLE `my_admin_rule`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` int(11) UNSIGNED NOT NULL DEFAULT 0,
  `src` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限节点' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_admin_rule
-- ----------------------------
INSERT INTO `my_admin_rule` VALUES (1, 0, '', '系统管理', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (5, 1, 'admin/conf/index', '系统配置', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (6, 5, 'admin/conf/add', '新增配置信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (7, 5, 'admin/conf/post_submit', '保存配置信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (8, 5, 'admin/conf/edit', '编辑配置详情', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (9, 5, 'admin/conf/conf_submit', '保存配置内容', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (10, 5, 'admin/conf/delete', '删除配置信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (11, 1, 'admin/menu/index', '功能菜单', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (12, 11, 'admin/menu/add', '添加菜单', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (13, 11, 'admin/menu/post_submit', '保存菜单信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (14, 11, 'admin/menu/delete', '删除菜单', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (15, 1, 'admin/rule/index', '功能节点', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (16, 15, 'admin/rule/add', '添加节点', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (17, 15, 'admin/rule/post_submit', '保存节点信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (18, 15, 'admin/rule/delete', '删除节点', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (19, 1, 'admin/role/index', '权限角色', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (20, 19, 'admin/role/add', '添加角色', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (21, 19, 'admin/role/post_submit', '保存角色信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (22, 19, 'admin/role/delete', '删除角色', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (23, 1, 'admin/admin/index', '管理员', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (24, 23, 'admin/admin/add', '添加/修改管理员', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (25, 23, 'admin/admin/post_submit', '保存管理员信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (26, 23, 'admin/admin/view', '查看管理员信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (27, 23, 'admin/admin/delete', '删除管理员', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (28, 1, 'admin/admin/log', '操作日志', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (29, 1, 'admin/database/database', '备份数据', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (30, 29, 'admin/database/backup', '备份数据表', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (31, 29, 'admin/database/optimize', '优化数据表', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (32, 29, 'admin/database/repair', '修复数据表', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (33, 1, 'admin/database/backuplist', '还原数据', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (34, 33, 'admin/database/import', '还原数据表', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (35, 33, 'admin/database/downfile', '下载备份数据', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (36, 33, 'admin/database/del', '删除备份数据', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (39, 0, '', '用户管理', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (40, 39, 'admin/user/index', '用户列表', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (41, 40, 'admin/user/edit', '编辑用户信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (42, 40, 'admin/user/post_submit', '保存用户信息', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (43, 40, 'admin/user/delete', '禁用用户', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (44, 39, 'admin/user/record', '操作记录', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (45, 39, 'admin/user/log', '用户日志', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (53, 15, 'admin/rule/edit', '编辑节点', '', 0, 0);
INSERT INTO `my_admin_rule` VALUES (54, 11, 'admin/menu/edit', '编辑菜单', '', 0, 0);

-- ----------------------------
-- Table structure for my_config
-- ----------------------------
DROP TABLE IF EXISTS `my_config`;
CREATE TABLE `my_config`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置名称',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置标识',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '配置内容',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态：-1删除 0禁用 1启用',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_config
-- ----------------------------
INSERT INTO `my_config` VALUES (1, '网站配置', 'web', 'a:12:{s:2:\"id\";s:1:\"1\";s:11:\"admin_title\";s:12:\"梦亚框架\";s:5:\"title\";s:12:\"梦亚框架\";s:4:\"logo\";s:52:\"/storage/202202/c5949d39a469d0a0b1b74cfb0fb04164.png\";s:6:\"domain\";s:20:\"https://www.mykc.com\";s:3:\"icp\";s:23:\"粤ICP备1xxxxxx11号-1\";s:8:\"keywords\";s:12:\"梦亚框架\";s:5:\"beian\";s:29:\"粤公网安备1xxxxxx11号-1\";s:4:\"desc\";s:262:\"梦亚框架是一套基于ThinkPHP6 + Layui + MySql打造的轻量级、高性能快速建站的内容管理系统。后台管理模块，一目了然，操作简单，通用型后台权限管理框架，紧随潮流、极低门槛、开箱即用。            \";s:4:\"code\";s:0:\"\";s:9:\"copyright\";s:32:\"© 2021 dream-kc.com MIT license\";s:7:\"version\";s:5:\"1.0.2\";}', 1, 1612514630, 1643897682);
INSERT INTO `my_config` VALUES (2, '邮箱配置', 'email', 'a:8:{s:2:\"id\";s:1:\"2\";s:4:\"smtp\";s:11:\"smtp.qq.com\";s:9:\"smtp_port\";s:3:\"465\";s:9:\"smtp_user\";s:15:\"gougucms@qq.com\";s:8:\"smtp_pwd\";s:6:\"123456\";s:4:\"from\";s:24:\"勾股CMS系统管理员\";s:5:\"email\";s:18:\"admin@gougucms.com\";s:8:\"template\";s:12:\"<p>11111</p>\";}', 1, 1612521657, 1643811756);
INSERT INTO `my_config` VALUES (3, '微信配置', 'wechat', 'a:11:{s:2:\"id\";s:1:\"3\";s:5:\"token\";s:8:\"GOUGUCMS\";s:14:\"login_back_url\";s:49:\"https://www.gougucms.com/wechat/index/getChatInfo\";s:5:\"appid\";s:18:\"wxdf96xxxx7cd6f0c5\";s:9:\"appsecret\";s:32:\"1dbf319a4f0dfed7xxxxfd1c7dbba488\";s:5:\"mchid\";s:10:\"151xxxx331\";s:11:\"secrect_key\";s:29:\"gougucmsxxxxhumabcxxxxjixxxng\";s:8:\"cert_url\";s:13:\"/extend/cert/\";s:12:\"pay_back_url\";s:42:\"https://www.gougucms.com/wxappv1/wx/notify\";s:9:\"xcx_appid\";s:18:\"wxdf96xxxx9cd6f0c5\";s:13:\"xcx_appsecret\";s:28:\"gougucmsxxxxhunangdmabcxxxng\";}', 1, 1612522314, 1613789058);
INSERT INTO `my_config` VALUES (4, 'Api Token配置', 'token', 'a:5:{s:2:\"id\";s:1:\"4\";s:3:\"iss\";s:16:\"www.dream-kc.com\";s:3:\"aud\";s:4:\"mykc\";s:7:\"secrect\";s:4:\"MYKC\";s:7:\"exptime\";s:4:\"3600\";}', 1, 1627313142, 1643897635);
INSERT INTO `my_config` VALUES (5, '其他配置', 'other', 'a:3:{s:2:\"id\";s:1:\"5\";s:6:\"author\";s:12:\"梦亚科创\";s:7:\"version\";s:13:\"v1.2021.07.28\";}', 1, 1613725791, 1643897618);

-- ----------------------------
-- Table structure for my_file
-- ----------------------------
DROP TABLE IF EXISTS `my_file`;
CREATE TABLE `my_file`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所属模块',
  `sha1` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'sha1',
  `md5` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'md5',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '原始文件名',
  `filename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) NOT NULL DEFAULT 0 COMMENT '文件大小',
  `fileext` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mimetype` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上传会员ID',
  `uploadip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `audit_time` int(11) NOT NULL DEFAULT 0 COMMENT '审核时间',
  `action` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '来源模块功能',
  `use` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT 0 COMMENT '下载量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_file
-- ----------------------------

-- ----------------------------
-- Table structure for my_user
-- ----------------------------
DROP TABLE IF EXISTS `my_user`;
CREATE TABLE `my_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户微信昵称',
  `nickname_a` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户微信昵称16进制',
  `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手机（也可以作为登录账号)',
  `mobile_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '手机绑定状态： 0未绑定 1已绑定',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '邮箱',
  `headimgurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信头像',
  `sex` tinyint(1) NOT NULL DEFAULT 0 COMMENT '性别 0:未知 1:女 2:男 ',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态  -1删除 0禁用 1正常 ',
  `last_login_time` int(11) NOT NULL DEFAULT 0 COMMENT '最后登录时间',
  `last_login_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `login_num` int(11) NOT NULL DEFAULT 0,
  `register_time` int(11) NOT NULL DEFAULT 0 COMMENT '注册时间',
  `register_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '注册IP',
  `wx_platform` int(11) NOT NULL DEFAULT 0 COMMENT '首次注册来自于哪个微信平台',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_user
-- ----------------------------

-- ----------------------------
-- Table structure for my_user_log
-- ----------------------------
DROP TABLE IF EXISTS `my_user_log`;
CREATE TABLE `my_user_log`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `type` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作类型',
  `title` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '操作描述',
  `module` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块',
  `controller` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '控制器',
  `function` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '方法',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录ip',
  `param_id` int(11) UNSIGNED NOT NULL COMMENT '操作ID',
  `param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参数json格式',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0删除 1正常',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户操作日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of my_user_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
