MYadmin V2开发框架
===============

> 运行环境要求PHP7.2+，兼容PHP8.1


Myadmin是一个基于ThinkPHP6的免费开源，快速简单，面向对象的应用研发架构，是为了快速研发应用而诞生。在保持出色的性能和新颖设计思想同时，也注重易用性。遵循Apache2开源许可协议发布，您可以免费使用Myadmin将研发的产品发布或销售，但不能未经授权抹除产品标志再次用于开源。

特别感谢如下项目的支持
===============

笔下光年：（http://www.bixiaguangnian.com/）

Bootstrap：（www.bootcss.com）


项目截图
===============
![](https://zzobokj.com/img/%E7%99%BB%E5%BD%95%E9%A1%B5%E9%9D%A2.png)
![](https://zzobokj.com/img/%E5%90%8E%E5%8F%B0.png)


演示地址
===============
[https://zzobokj.com/admin/](https://zzobokj.com/admin/)

后台账号：`admin`  密码：`123456`

安装教程
===============

克隆项目：

`git clone https://gitee.com/dream-kc/myadminV2.git`

导入数据库：

根目录：`sj.sql`

修改配置：

修改：`config/database.php`文件数据库配置信息

安装依赖包：

`composer install`

安装完成！

访问后台：`http://你的域名/admin`

默认账户密码：`admin aini201`

APIDOC地址：`http://你的域名/APIDOC`


**大家用的好的话麻烦点个star！谢谢**